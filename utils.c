#include "utils.h"


void my_free (void *data, void *hint)
{
 free (data);
}

int random_(int min, int max){
   return rand() %(max-min+1) + min ; 
}
void affiche(int n, int m, int **A){
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            printf("%5d ",A[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
void free_M(int n1, int n2, int **A, int **B, int **C){
    for (int i = 0; i < n1; ++i)
    {
        free(A[i]);
        free(C[i]);
    }
    for (int i = 0; i < n2; ++i)
    {
        free(B[i]);
    }

    free(A);
    free(B);
    free(C);
}

void init(int n, int m, int **A){
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            A[i][j] = random_(0,10);
        }
    }
}
int produit_scalaire(int n, int *A, int *B){
    int p = 0;
    for (int i = 0; i < n; ++i)
    {
        p+=A[i]*B[i];
    }

    return p;
}
void define_data(int n1, int m1, int n2, int m2, int **A, int **B)
{
    
	srand(time(0));
	init(n1, m1, A);
	init(n2, m2, B);

}
int * line(int n,int m, int **T, int l){
    int * R = (int*) malloc(m*sizeof(int));
	for (int i = 0; i < m; ++i)
	{
		R[i] = T[l][i];
	}
    return R;
}
int * colonne(int n,int m, int **T, int c){
    int * R = (int*) malloc(n*sizeof(int));
	for (int i = 0; i < n; ++i)
	{
		R[i] = T[i][c];
	}
    return R;
}