#include "utils.h"

typedef struct {
      int debut;
      int fin;
   }Argument;

void * calculScalaires(void *a);

int n1, n2, m1, m2, Nthr;
int **A, **B, **C;

int main(int argc, char const *argv[])
{
	// définition des paramètres. 
    Nthr = atoi(argv[1]);
	n1 = atoi(argv[2]);
    m1 = atoi(argv[3]);
    n2 = atoi(argv[4]);
    m2 = atoi(argv[5]);

    if(m1 != n2) exit(-1);


    // initialisation des matrices 

    A = (int **) malloc (n1*sizeof(int*));
    B = (int **) malloc (n2*sizeof(int*));
    C = (int **) malloc (n1*sizeof(int*));

    for (int i = 0; i < n1; ++i)
    {
        A[i] = (int*) malloc(m1*sizeof(int));
        C[i] = (int*) malloc(m2*sizeof(int));
    }
    for (int i = 0; i < n2; ++i)
    {
        B[i] = (int*) malloc(m2*sizeof(int));
    }

	define_data(n1, m1, n2, m2, A, B);




// creation des threads

	pthread_t threads[Nthr];
    Argument argument[Nthr];

    top_(&w_time_par1,&tz);
    for (int i = 0; i < Nthr; ++i)
    {   
        Argument *ar = &argument[i];
        int fraction = (int) ((n1*m2) / Nthr);
        ar->debut = i*fraction;
        ar->fin = i == (Nthr - 1) ? n1*m2 : ar->debut + fraction; 
        pthread_create(&threads[i], NULL, calculScalaires, (void*)ar);
    }


    for( int i = 0 ; i < Nthr; i++){
        pthread_join(threads[i], NULL);
    }

  


    top_(&w_time_par2,&tz);

    printf("----------------------------------------------------------------------\n\n");
    affiche(n1,m1,A);
    affiche(n2,m2,B);
    printf("----------------------------------------------------------------------\n\n");

    affiche(n1,m2,C);
    
    free_M(n1,n2,A,B,C);
    

    printf("\n\n");
    work_time_par = cpu_time_(w_time_par1, w_time_par2);

    FILE *fi = fopen("posix.csv","a");
    fprintf(fi,"%d %lld.%03lld\n",Nthr, work_time_par/1000, work_time_par%1000);
    printf("%d %lld.%03lld (ms) (posix) \n",Nthr, work_time_par/1000, work_time_par%1000);
    fclose(fi);

    sleep(1);
	return 0;
}

void * calculScalaires(void *a){
    Argument * ar = (Argument*) a ;
    int debut = ar->debut; 
    int fin = ar->fin;
    int i,j;
    for (int t = debut; t < fin; ++t)
    {  
        i = t/m2;
        j = t%m2;
        int * lin = line(n1,m1,A,i);
        int * col = colonne(n2,m2,B,j);
        C[i][j] = produit_scalaire(m1,lin,col);
        free(lin);
        free(col);
    }
}