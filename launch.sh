#!/bin/bash

n1=63
m1=100000
n2=100000
m2=63
n_iter=10

for k in `seq 0 7`
do
	let c=k+1
	for i in `seq 1 $n_iter`
	do
		./posix $c $n1 $m1 $n2 $m2
		./zmq $c $n1 $m1 $n2 $m2
		./zmq_nocopy $c $n1 $m1 $n2 $m2
    done
done
