#ifndef _UTILS
#define _UTILS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include "time_manager.h"
#include "zhelpers.h"

unsigned long long work_time_par;
timeval_t w_time_par1, w_time_par2, tz;

int random_(int , int);
void affiche(int n, int m, int **A);
void init(int n, int m, int **A);
int produit_scalaire(int n, int *A, int *B);
void my_free (void *data, void *hint);
void define_data(int n1, int m1, int n2, int m2, int **A, int **B);
int * line(int n,int m, int **T, int l);
int * colonne(int n,int m, int **T, int c);
void free_M(int n1, int n2, int **A, int **B, int **C);



#endif