#include "utils.h"


int n1, n2, m1, m2, Nthr;
int **A, **B, **C;


void * calculScalaires(void * context);

int main(int argc, char const *argv[])
{

    // Création des Sockets. 

     void *context = zmq_ctx_new ();
    //  Socket to send messages on
    void *sender = zmq_socket (context, ZMQ_PUSH);
    zmq_bind (sender, "inproc://endpoint1");

    //  Socket receive result
    void *receiver = zmq_socket (context, ZMQ_PULL);
    zmq_bind (receiver, "inproc://endpoint2");

    // Socket pour envoyer le signal d'arret à tous les threads
    void *publisher = zmq_socket(context, ZMQ_PUB);
    zmq_bind(publisher, "inproc://endpoint3");


    // définition des paramètres. 

    Nthr = atoi(argv[1]);
    n1 = atoi(argv[2]);
    m1 = atoi(argv[3]);
    n2 = atoi(argv[4]);
    m2 = atoi(argv[5]);

    if(m1 != n2) exit(-1);

    // initialisation des matrices

    A = (int **) malloc (n1*sizeof(int*));
    B = (int **) malloc (n2*sizeof(int*));
    C = (int **) malloc (n1*sizeof(int*));

    for (int i = 0; i < n1; ++i)
    {
        A[i] = (int*) malloc(m1*sizeof(int));
        C[i] = (int*) malloc(m2*sizeof(int));
    }
    for (int i = 0; i < n2; ++i)
    {
        B[i] = (int*) malloc(m2*sizeof(int));
    }

    define_data(n1, m1, n2, m2, A, B);

// creation des threads
    pthread_t threads[Nthr];
    for (int i = 0; i < Nthr; ++i)
    {
        pthread_create(&threads[i], NULL, calculScalaires, context);
    }


// Envoie des données aux workers
    top_(&w_time_par1,&tz);

    for (int i = 0; i < n1; ++i)
    {
        for (int j = 0; j < m2; ++j)
        {
            int * val1 = line(n1,m1,A,i);
            int * val2 = colonne(n2,m2,B,j);
            int * val3 = (int*) malloc(2*sizeof(int));
            val3[0] = i;
            val3[1] = j;
            zmq_send(sender, val1, m1*sizeof(int), ZMQ_SNDMORE);
            zmq_send(sender, val2, n2*sizeof(int), ZMQ_SNDMORE);
            zmq_send(sender, val3, 2*sizeof(int), 0);
            free(val1);
            free(val2);
            free(val3);

        }
    }
   

    // On recupère les infos et on envoie un signale pour tuer les workers
    int * reps;
    reps = (int*) malloc(3*sizeof(int));

    for(int i =0 ; i < n1*m2 ; i++){

        zmq_recv(receiver, reps, 3*sizeof(int), 0);
        C[reps[0]][reps[1]] = reps[2];
    }

    top_(&w_time_par2,&tz);
    // on envoie un signal d'arret au differents threads
    s_send(publisher,"KILL");

    printf("----------------------------------------------------------------------\n\n");
    affiche(n1,m1,A);
    affiche(n2,m2,B);
    printf("----------------------------------------------------------------------\n\n");

    affiche(n1,m2,C);
    printf("\n\n");

    free_M(n1,n2,A,B,C);

    
    work_time_par = cpu_time_(w_time_par1, w_time_par2);
    FILE *fi = fopen("zmq.csv","a");
    fprintf(fi,"%d %lld.%03lld\n",Nthr, work_time_par/1000, work_time_par%1000);
    fclose(fi);
    printf("%d thread : %lld.%03lld(ms) (zmq)\n",Nthr, work_time_par/1000, work_time_par%1000);

    zmq_close (receiver);
    zmq_close (sender);
    zmq_close (publisher);
    zmq_ctx_destroy (context);
    sleep(1);


    return 0;
}


void * calculScalaires(void *context){
    void *receiver = zmq_socket (context, ZMQ_PULL);
    zmq_connect (receiver, "inproc://endpoint1");
    void *subscriber = zmq_socket(context, ZMQ_SUB);
    zmq_connect(subscriber, "inproc://endpoint3");
    zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE,"",0);
    void *sender = zmq_socket (context, ZMQ_PUSH);
    zmq_connect (sender, "inproc://endpoint2");

    zmq_pollitem_t items [] = {
        { receiver,   0, ZMQ_POLLIN, 0 },
        { subscriber, 0, ZMQ_POLLIN, 0 }
    };


    //  Process tasks forever
    int *val1,*val2, *val3, *r;
    val1 = (int*) malloc(m1*sizeof(int));
    val2 = (int*) malloc(n2*sizeof(int));
    val3 = (int*) malloc(2*sizeof(int));
    r = (int*) malloc(3*sizeof(int));   
    while (1) {

        zmq_poll (items, 2, -1);
        if (items [1].revents & ZMQ_POLLIN) {
            char * ligne = s_recv(subscriber);
            if (ligne !=NULL) {
                //printf("arret du threads\n");
               break;
            }
        }
        if (items [0].revents & ZMQ_POLLIN){
            zmq_recv(receiver, val1, m1*sizeof(int), ZMQ_RCVMORE);
            zmq_recv(receiver, val2, n2*sizeof(int), ZMQ_RCVMORE);
            int size = zmq_recv (receiver, val3, 2*sizeof(int), 0);

            if (size != -1)
            {
                int j = val3[1];
                int i = val3[0];
                int p = produit_scalaire(m1,val1,val2);
                
                r[0] = i;
                r[1] = j;
                r[2] = p;
                zmq_send (sender, r, 3*sizeof(int), 0);

            }
        }

    }
    free(val1);
    free(val2);
    free(val3);
    free(r);
    zmq_close (receiver);
    zmq_close (sender);
    zmq_close (subscriber);
}